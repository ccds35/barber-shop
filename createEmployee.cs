﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace beautysystem
{
    public partial class createEmployee : Form
    {
        public createEmployee()
        {
            InitializeComponent();
        }

        private void createEmployee_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string id = textBox1.Text;
            string name = textBox2.Text;
            string gender = textBox3.Text;
            string ipone = textBox4.Text;
            string project = textBox5.Text;
            string connStr = "server=127.0.0.1;database=beautiful_system;uid=test;pwd=test!";
            SqlConnection conn = null;
            try
            {
                conn = new SqlConnection(connStr);
                conn.Open();

                string sql = "INSERT into createEmployee (id,name,gender,ipone,project) values ";
                SqlParameter[] pms =
                    {
                new SqlParameter("@id", SqlDbType.NVarChar, 10),
                new SqlParameter("@name", SqlDbType.NChar, 10),
                new SqlParameter("@gender", SqlDbType.NChar, 10),
                new SqlParameter("@ipone",SqlDbType.NChar,10),
                new SqlParameter("@project",SqlDbType.NChar,10),
                };
                pms[0].Value = id;
                pms[1].Value = name;
                pms[2].Value = gender;
                pms[3].Value = ipone;
                pms[4].Value = project ;

                sql = sql + "('" + id + "','" + name + "','" + gender + "','" + ipone + "','" + project + "')";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
                MessageBox.Show("插入成功！");

                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show("插入失败！" + ex.Message);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }
    }
}
